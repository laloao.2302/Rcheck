require_relative "rcheck/version"
require_relative "rcheck/cli"
require_relative "rcheck/cli/options"
require_relative "rcheck/request"
require_relative "rcheck/request/ping"
require_relative "rcheck/report"

# Entry point of the program.
# This redirects the arguments to the respective module
module Rcheck
  # This is so the file doesn't run if required
  if __FILE__ == $0
    Rcheck::CLI.run(ARGV) 
  end
end
