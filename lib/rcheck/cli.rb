module Rcheck
  module CLI
    module_function

    # This function is the entry point for command line applications
    # Algortihm:
    #   - Parses the options
    #   - Makes the necessary requests
    #   - Reports the average result
    def run(opts)
      parsed_options = Rcheck::CLI::Options.parse(opts)

      puts "Working... #{parsed_options[:website]}"
      puts "Making a request every #{parsed_options[:time]} sec. for #{parsed_options[:duration]} sec."

      pings = Rcheck::Request.make_requests(parsed_options)
      average = Rcheck::Report.average(pings)

      if parsed_options[:csv]
        Rcheck::Report.csv(pings, parsed_options[:csv])
      end

      print_average(average)
    end

    # Shows status of each individial request. This is called in --verbose mode.
    # params:
    #   - +ping+: Ping object to report
    def print_verbose(ping)
      message = Rcheck::Report.verbose(ping)
      puts message
    end

    # Shows the average response time of all the requests made
    # params:
    #   - +average+: average time in milliseconds
    def print_average(average)
      avrg = (average * 100).round(2)
      puts "\nAverage time of response was #{avrg} milliseconds."
    end
  end
end
