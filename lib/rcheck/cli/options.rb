require "optparse"
module Rcheck
  module CLI
    module Options
      module_function

      # Parses and cleans the arguments from the command line.
      # The configuration of the options is made here
      def parse(argv)
        options = {
          :website => "https://about.gitlab.com",
          :time => 4,
          :duration => 20,
          :verbose => false,
          :csv => false
        }

        opts_parser = OptionParser.new do |opts|
          opts.banner = "Usage: ruby lib/rcheck.rb <website> [options]\nDefault website: https://about.gitlab.com"

          opts.on("-v", "--verbose", "Shows information of every request") do
            options[:verbose] = true
          end

          opts.on("-t TIME", "--time TIME", Integer, 
                  "Set time between requests in seconds. (Default to 10 sec.)") do |time|
            options[:time] = time
          end

          opts.on("-d DURATION", "--duration DURATION", Integer,
                  "Set total time spending on making requests in seconds. (Default to 60 seconds)") do |duration|
            options[:duration] = duration
          end

          opts.on("--csv FILENAME", String, "Exports data of each request into a csv file") do |filename|
            options[:csv] = filename
          end
        end

        opts_parser.parse!(argv)

        unless argv.empty?
          options[:website] = argv[0]
        end

        options
      end
    end
  end
end
