require "net/http"
class Ping
  attr_accessor :id, :website, :code, :time
  # == Ping
  # This class has the metadata of a request
  # attrs:
  #   - +id+: Number of request
  #   - +website+: Target of request
  #   - +code+: Response code of request
  #   - +time+: How much time the request took

  def initialize(website, id = 0)
    @website = website
    @id = id
  end

  def run
    uri = URI(@website)
    start_time = Time.now
    begin
      response = Net::HTTP.get_response(uri)
      @code = response.code
    rescue
      @code = "CONNECTION REFUSED"
    end
    end_time = Time.now
    @time = end_time - start_time 
  end

  # Returns the names of the class attributes
  def attrs_name_array
    ["id", "website", "code", "time"]
  end

  # Returns the values of the class as an array
  def info_array
    [@id, @website, @code, @time]
  end
end
