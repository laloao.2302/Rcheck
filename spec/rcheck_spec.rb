require 'spec_helper'

# This file contains BDD tests. This behaves like the end user.
# Dependencies on this tests are not mocked, for the pourpose of
# simulating at its best how the system will respond to the end user
# in the real world.
RSpec.describe Rcheck, :type => :aruba, :io_wait_timeout => 30 do
  context "Running WITHOUT flags, arguments or options" do
    let(:command_s) {"ruby #{Dir.pwd}/lib/rcheck.rb"}
    before(:each) { stop_all_commands }
    before(:each) { run(command_s) }
    let(:command) { last_command_started }

    it "has a version number" do
      expect(Rcheck::VERSION).not_to be nil
    end

    it "shows working message" do
      expect(command.output).to include("Working... https://about.gitlab.com")
    end

    it "shows the average time of requests" do
      expect(command.output).to include("Average time of response was")
    end
  end

  context "Running WITH -v flag" do
    let(:command) {"ruby #{Dir.pwd}/lib/rcheck.rb -v"}
    before(:each) { stop_all_commands }
    before(:each) { run(command) }

    it "returns website status" do
      expect(last_command_started.output).to include("https://about.gitlab.com has responded with") 
    end

    # TODO: Should be 10 seconds
    # Now testing at 4 seconds
    it "makes a request every n seconds" do
      result = last_command_started.output.split("\n")

      expect(result.count).to be > 2
    end

    # TODO: Should be 1 min. as default
    # Now testing at 20 sec
    it "keeps making requests for n time" do
      result = last_command_started.output.split("\n")

      expect(result.count).to be >= 5
    end
  end
  
  context "Running with a website given" do
    let(:website) { "https://gitlab.com" }
    let(:command) { "ruby #{Dir.pwd}/lib/rcheck.rb #{website}" }
    before(:each) { stop_all_commands }
    before(:each) { run(command) }

    it "makes requests to given website" do
      expect(last_command_started.output).to include(website)
    end
  end

  context "Running with --time TIME flag" do
    let(:command) { "ruby #{Dir.pwd}/lib/rcheck.rb -t 1" }
    before(:each) { stop_all_commands }
    before(:each) { run(command) }

    it "shows every second is making a request" do
      expect(last_command_started.output).to include("every 1 sec.")
    end
  end

  context "Running with --duration DURATION flag" do
    let(:command) { "ruby #{Dir.pwd}/lib/rcheck.rb -d 10" }
    before(:each) { stop_all_commands }
    before(:each) { run(command) }

    it "shows the duration of the process will be of 10 seconds" do
      expect(last_command_started.output).to include("for 10 sec.")
    end
  end
end
