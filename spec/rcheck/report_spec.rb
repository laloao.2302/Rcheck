require 'spec_helper'

RSpec.describe Rcheck::Report do
  context "#average/1" do
    it "returns the average formated in milliseconds #average/1" do
      pings = []
      6.times do |n|
        p = Ping.new("a website")
        if n.odd?
          p.time = 0.3435 
        else
          p.time = 0.3232
        end
         pings << p
      end

      result = Rcheck::Report.average(pings)
      should_be = 0.33335

      expect(result.round(5)).to be should_be
    end
  end

  context "#csv/2" do
    let(:pings) { (1..3).reduce([]) { |acc, x| 
                          p = Ping.new("a website", x)
                          p.code = "200"
                          p.time = 0.33
                          acc << p }
    }
    let(:filename) { "temp" }
    let(:path) { File.join(Dir.pwd, "temp.csv") }
    after(:each) { File.delete(path) }
    it "creates a csv file" do
      Rcheck::Report.csv(pings, filename) 

      expect(File).to exist(File.join(Dir.pwd, filename + ".csv"))
    end

    it "writes the info of all the requests" do
      Rcheck::Report.csv(pings, filename)

      lines = File.readlines(path)

      expect(lines.count).to be == 4
    end
    
    it "writes correctly the info of a request" do
      Rcheck::Report.csv(pings, filename)

      lines = File.readlines(path)
      info = lines[1].split(",").map { |x| x.strip }
      ping_info = pings[0].info_array.map { |x| x.to_s }

      expect(info).to eq(ping_info)
    end

    it "writes the headers correctly" do
      Rcheck::Report.csv(pings, filename)
      lines = File.readlines(path)

      info = lines[0].split(",").map { |x| x.strip }
      ping_info = pings[0].attrs_name_array

      expect(info).to eq(ping_info)
    end
  end

  context "#verbose/1" do
    let(:ping) { 
      p = Ping.new("a website")
      p.time = 0.33
      return p
    }

    it "returns message of success" do
      ping.code = "200"

      result = Rcheck::Report.verbose(ping)
      should_be = "a website has responded with success (200)"

      expect(result).to eq(should_be)
    end

    it "returns a message with another code" do
      ping.code = "400"

      result = Rcheck::Report.verbose(ping)
      should_be = "a website has responded with code 400"

      expect(result).to eq(should_be)
    end
  end
end
