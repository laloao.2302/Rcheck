require "spec_helper"

RSpec.describe Ping do
  context "#initialize/2" do
    it "assigns a website" do
      name = "website"

      p = Ping.new(name)

      expect(p.website).to eq(name)
    end

    it "returns array with all its values" do
      p = Ping.new("a website", 3)
      p.code = "200"
      p.time = 0.33

      result = p.info_array
      should_be = [3, "a website", "200", 0.33]

      expect(result).to eq(should_be)
    end

    it "returns array with name of attributes" do
      p = Ping.new("a website")

      result = p.attrs_name_array
      should_be = ["id", "website", "code", "time"]

      expect(result).to eq(should_be)
    end
  end
end
