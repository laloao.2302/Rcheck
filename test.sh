#!/bin/bash

function run_unit_tests {
    rspec spec/rcheck/*.rb
    rspec spec/rcheck/request/*.rb
}

function run_bdd_tests {
    rspec spec/rcheck_spec.rb
}

function show_help {
    echo "Do you want to run unit or bdd tests? u/b"
    read opt

    case $opt in
      u)
          run_unit_tests
        ;;
      b)
          run_bdd_tests
        ;;
      *)
        show_help
    esac
}

case "$1" in
  "unit")
    run_unit_tests
    ;;
  "bdd")
    run_bdd_tests
    ;;
  *)
    show_help
esac

